---
title: "Volební mapa Brna: dominance lidovců dokáže přežít sto let a dvě diktatury"
perex: "Politická paměť některých městských částí je nečekaně dlouhá. Dnešní lokální podporu křesťanských demokratů lze vystopovat až do prvních voleb za Rakouska-Uherska. Podobné vzorce platí i pro komunisty."
description: "Politická paměť některých městských částí je nečekaně dlouhá. Dnešní lokální podporu křesťanských demokratů lze vystopovat až do prvních voleb za Rakouska-Uherska. Podobné vzorce platí i pro komunisty."
authors: ["Jan Boček", "Jan Cibulka"]
published: "14. září 2016"
socialimg: https://interaktivni.rozhlas.cz/historie-brno/media/socimg.png
coverimg: https://interaktivni.rozhlas.cz/historie-brno/media/coverimg.jpg
coverimg_note: "Foto: Obecní volby 1954, průvod mládeže v Brně, archiv ČTK"
url: "historie-brno"
libraries: [leaflet]
recommended:
  - link: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/
    title: Doleva či doprava. Podívejte se, jak volí vaše obec
    perex: Které strany zde vedou a které naopak propadají?
    image: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
  - link: https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/
    title: Český rozhlas představuje kandidáty do krajských zastupitelstev
    perex: Šest otázek a stejný časový limit. Představuje se všech 233 kandidátů do krajů.
    image: https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/media/mapa.png
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany
    perex: Prohlédněte si krajské kandidátní listiny v grafech
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
---

Žebětín na okraji Brna volí od roku 1989 stále stejnou stranu: v komunálních volbách tu bez výjimky vítězí křesťanští demokraté. Jejich nejslabší volební výsledek za uplynulé čtvrtstoletí byl těsně pod 60 procenty hlasů, stejný starosta už slouží páté volební období, úřad mu předával jeho otec.

<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/volby-2016-historie-brno/www/media/lidovci1911.png" width="300" height="280">
  </figure>
</aside>

Stejné straně věřili už pradědečci a prababičky dnešních Žebětíňanů. Za první republiky, kdy byl ještě Žebětín samostatnou obcí, tu Československá strana lidová válcovala politickou konkurenci podobně jako dnes. Dokonce ještě dřív, v prvních všeobecných a rovných volbách do předlitavské Říšské rady v roce 1907, tu katolický kandidát Arnošt Gottwald porazil sociálního demokrata Toužila i mladočecha Bulína. (Navzdory přívlastkům *všeobecné* a *rovné* tehdy směli volit pouze muži, české ženy šly poprvé k volbám až v roce 1919.)

<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/volby-2016-historie-brno/www/media/lidovci2006.png" width="300" height="280">
  </figure>
  <figcaption>
    Rozložení podpory lidovců ve volbách do Říšské rady 1911 a Poslanecké sněmovny PČR 2006. Zdroj: <a href="http://www.cdk.cz/knihy/srovnavaci-politologie-ispo/volebni-mapa-mesta-brna">Volební mapa města Brna</a>
  </figcaption>
</aside>

Brněnská Líšeň má zase v genech podporu komunistů. V parlamentních volbách v roce 1929 – osm let po odloučení KSČ od mateřských sociálních demokratů – tady komunisti dosáhli jednoho z nejlepších výsledků v Brně a blízkém okolí. Těsně po válce, kdy už byla Líšeň součástí města, svůj úspěch zopakovali.

O sedmdesát let později tu nereformovaná KSČM získává ve volbách do Poslanecké sněmovny i krajských zastupitelstev pravidelně o tři procentní body víc, než je ve městě obvyklé. Prvorepubliková dělnická tradice přežívala ve Zbrojovce a později Zetoru, v sedmdesátých letech přibylo také normalizační sídliště. Silnou podporu ale komunisti mají i ve Staré Líšni, kde nerušeně přežívají venkovské tradice a vztah k minulosti je stále živý.

## Za ztrátu paměti může vysídlení nebo sídliště

<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/volby-2016-historie-brno/www/media/komunisti1929.png" width="300" height="280">
  </figure>
</aside>

Zdaleka ne všude volební paměť vydržela. Naopak, jde spíš o výjimečný jev. Tam, kde došlo k přerušení historických vzorců, ale obvykle lze odhadnout dobu i příčinu změny. V Brně je to nejčastěji vyhnání německého obyvatelstva z jižní části města s následným dosídlením vyprázdněných čtvrtí nebo pak komunistická přeměna vesnických čtvrtí na panelová sídliště. Často – třeba v Komárově – působí oba prvky společně. K nim se v této čtvrti ještě přidává rozdělení městské části čtyřpruhovou silnicí na dvě části, které spolu nekomunikují.

<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/volby-2016-historie-brno/www/media/komunisti2006.png" width="300" height="280">
  </figure>
  <figcaption>
    Rozložení podpory komunistů ve volbách do Poslanecké sněmovny v letech 1929 a 2006. Zdroj: <a href="http://www.cdk.cz/knihy/srovnavaci-politologie-ispo/volebni-mapa-mesta-brna">Volební mapa města Brna</a>
  </figcaption>
</aside>

Kliknutím do mapy můžete objevovat, jak městské části volí dnes – jednak v komunálních, jednak v krajských a parlamentních volbách. Obvykle jsou v obou případech trendy podobné; pokud je strana v některé městské části silná při volbě starosty, bývá zde silná i ve „velkých“ volbách. Kupodivu to ale neplatí všude, výjimkou jsou například sídliště na jihozápadě města, Bohunice a Starý Lískovec. V krajských a parlamentních volbách tu sice „podle pravidel sídliště“ mají převahu levicové strany, ale v obecních volbách překvapivě vítězí ODS nebo KDU-ČSL.

Tam, kde existuje návaznost nebo naopak evidentní přetržka historických vzorců, o ní píšeme. Pokud městská část v minulosti nefandila žádnému politickému proudu, volební minulost nezmiňujeme.

Tmavomodrá barva na mapě označuje lidnatější části, světle modrá místa s méně obyvateli. Nejvíc voličů (a tedy největší váhu v parlamentních nebo krajských volbách) má Brno-střed, kde může k volbám dorazit 67 tisíc lidí, nejméně vlivný je Ořešín s čtyřmi stovkami voličů.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/kraj-volby-brno-map/" class="ig" width="1024" height="400" scrolling="no" frameborder="0"></iframe>
</aside>

## Komunistům Brno nepřeje, neuspěli ani těsně po válce

Jako celek se Brno chová poměrně předvídatelně. Daří se tu spíše pravicovým stranám (je to město) a křesťanským demokratům (leží na jižní Moravě). Relativně dobré výsledky tu mají také menší pravicové a liberální strany: zelení, piráti, svobodní nebo ještě nedávno SNK ED.

Nedaří se zde naopak KSČM. To není nic překvapivého, komunisté bývají úspěšnější na venkově. Podle politologa Michala Pinka z Masarykovy univerzity má ale neochota volit komunisty v Brně historické kořeny.

„Brno patří k lokalitám, kde se komunistům tradičně nedaří,“ tvrdí Pink. „Dobrým důkazem je rok 1946, kdy komunisté ovládli mnoho větších měst, v Brně ale neuspěli.“

Další proměnnou, která ukazuje na volební chování města, je volební účast. Ta může být klíčová v nadcházejících krajských volbách: slabší účast obvykle znamená vyšší podporu pro levicové strany, hlavně komunisty. Přitom právě v krajských volbách bývá účast už tradičně nízká.

Publikace Volební mapa města Brna upozorňuje na brněnské specifikum: volební účast je nejvyšší na severozápadě města, směrem k jihovýchodu klesá. Nejvyšší bývá tradičně v Jehnicích a Ivanovicích, nejméně volby zajímají Brno-jih, Bohunice a Chrlice.

Poslední zajímavostí jsou městské části, kde, podobně jako v Žebětíně, delší dobu dominuje jedna strana. Dlouhodobě se daří například hnutí Tři oříšky pro Lískovec Jany Drápalové (bývalé předsedkyně zelených), které v posledních volbách získalo víc než padesát procent hlasů. V Židenicích zase několikrát v řadě zvítězili křesťanští demokraté v čele se Stanislavem Juránkem. Tomu křeslo starosty pomohlo získat mandát hejtmana a později senátora.

„Jde o takzvaný sousedský efekt,“ vysvětluje politolog Pink. „Tam, kde je politik doma, dokáže své straně zajistit někdy i výrazně vyšší volební zisk.“

Velmi silně může právě domácí prostředí ovlivnit volby v okrajových městských částech, kde volí řádově jen stovky lidí. V roce 2002 díky němu získal křeslo útěchovského starosty tehdejší předseda republikánů Miroslav Sládek.

„V Útěchově jde o sousedský efekt osob, které si Miroslav Sládek bere na procházku v neděli odpoledne,“ doplňuje Pink.






