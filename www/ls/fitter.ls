document.querySelector \a#scroll-btn .addEventListener \click (evt) ->
    evt.preventDefault!
    content = document.querySelector \#content
    startOffset = window.pageYOffset || document.documentElement.scrollTop
    targetOffset = ig.utils.offset content .top
    offsetDifference = targetOffset - startOffset
    start = Date.now!
    duration = 600
    scrollStep = ->
      fromStart = Date.now! - start
      percentage = fromStart / duration
      percentage = 1 if percentage > 1
      scrollTo = startOffset + offsetDifference * Math.sin percentage * (Math.PI / 2)
      window.scrollTo 0, scrollTo
      if percentage < 1
        window.requestAnimationFrame scrollStep
    window.requestAnimationFrame scrollStep


for el in document.querySelectorAll "a[href^='http:'],a[href^='https:']"
  continue if el.id is "logo"
  continue if el.classList?contains "share"
  el.addEventListener \click (evt) ->
    console.log \click
    evt.preventDefault!
    window.open @getAttribute \href

for el in document.querySelectorAll "a.share"
  el.addEventListener \click (evt) ->
    evt.preventDefault!
    window.open do
      @getAttribute \href
      ''
      "width=550,height=265"

isAtTop = yes
header = document.querySelector "body > header"
isWithoutTopImage = document.querySelector '#opener' .classList?contains "no-cover-img"

setHeaderClass = ->
  top = (document.body.scrollTop || document.documentElement.scrollTop)
  triggerHeight = if isWithoutTopImage then 1 else window.innerHeight
  if top >= triggerHeight and isAtTop
    header.classList?remove \at-top
    isAtTop := no
  else if top < triggerHeight and not isAtTop
    header.classList?add \at-top
    isAtTop := yes

window.addEventListener \scroll setHeaderClass
setHeaderClass!
